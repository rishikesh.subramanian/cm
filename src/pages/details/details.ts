import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {GetProvider} from '../../providers/get/get';
import {HomePage} from '../home/home';
/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  tags = [];
  title: String;
  description: String;
  github:String;
  booleanTags = false;
  all:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController,
              public getServ: GetProvider ) {
                
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

  makeTagsList() {
    let prompt = this.alertCtrl.create({
      title: 'Add tag',
      inputs: [{
        name: 'tag',
        placeholder: "Add tag"
      }],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Add',
          handler: data => {
            console.log(data['tag']);
            // console.log("here^");
            this.tags.push(data['tag']);
          }
        }
      ]
    });

    prompt.present();
    this.booleanTags = true;
    console.log(this.tags);
  }

  deleteTag(i: number) {
    this.tags.splice(i, 1);
    console.log(this.tags);
}

  submit(){
    var tempJSON = {
      title: this.title,
      description: this.description,
      git: this.github,
      tags: this.tags
    }
    console.log(tempJSON);
    this.getServ.makeEntry(tempJSON).then((res => {
      // console.log('appointment set');
      console.log(tempJSON);
      // this.navCtrl.setRoot(HomePage);
    }), (err) => {
      console.log(err);
    });
    this.navCtrl.setRoot(HomePage);

  }

}
