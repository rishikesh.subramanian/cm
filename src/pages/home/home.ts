import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import {GetProvider} from '../../providers/get/get';
import { ViewPage } from '../view/view';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  all:any;
  constructor(public navCtrl: NavController, public getServ: GetProvider, public alertCtrl:AlertController) {
    this.getServ.getAll().then((res) => {
      this.all = res;
      console.log(this.all);
    }, (err) => {
      console.log(err);
    })

  }

  
  goToDetailsPage(){
    this.navCtrl.push(DetailsPage);
  }

  showMore(i){
    // const alert = this.alertCtrl.create({
    //   title: this.all[i]['github'],
    //   subTitle:this.all[i]['tags'],
    //   buttons: ['OK']
    // });
    // alert.present();
    
    this.navCtrl.push(ViewPage,{all:this.all[i]});
  }
  

}
