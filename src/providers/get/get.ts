import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// import { AuthProvider } from '../auth/auth';
// import { CONFIG } from '../../../config/config';
/*
  Generated class for the PatientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GetProvider {
  API_URL = "http://localhost:3000";
  constructor(public http: Http) {
  }
  getAll(){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.get(this.API_URL + '/getAll', { headers: headers })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, (err) => {
          reject(err);
        });

    });
  }

  makeEntry(entry){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.API_URL+"/makeEntry" , JSON.stringify(entry), { headers: headers })
        .subscribe(res => {
          let data = res.json();
          // this.token = data.token;
          // this.storage.set('token', data.token);
          resolve(data);
          resolve(res.json());
        }, (err) => {
          reject(err);
        });

    });

  }
}
